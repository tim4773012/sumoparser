## v 1.0 just kind of works as-is

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import pandas as pd

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///site.db'
db = SQLAlchemy(app)

class Account(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    account_id = db.Column(db.String(255))
    account_name = db.Column(db.String(255))

def import_data(file_path):
    df = pd.read_excel(file_path)
    for index, row in df.iterrows():
        account = Account(account_id=row['AccountID'], account_name=row['AccountName'])
        db.session.add(account)

    db.session.commit()

if __name__ == '__main__':
    with app.app_context():
        db.create_all()  # This will create the database tables
        import_data('db.xlsx')
