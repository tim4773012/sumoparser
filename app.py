## v 0.0.5 / requires db/site.db to be in the same directory as app.py 

from flask import Flask, render_template, request
import json
from cs50 import SQL

app = Flask(__name__)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///db/site.db")  # Change the database path as needed

def replace_keys(data, replacements):
    for old_key, new_key in replacements.items():
        if old_key in data:
            data[new_key] = data.pop(old_key)
    return data

def get_account_name(account_id):
    account_id = account_id.upper()
    result = db.execute("SELECT account_name FROM account WHERE account_id = :account_id", account_id=account_id)
    return result[0]["account_name"] if result else "Account not found"

def parse_sumo_data(sumo_data):
    try:
        if isinstance(sumo_data, str):
            data = json.loads(sumo_data)
        elif isinstance(sumo_data, dict):
            data = sumo_data
        else:
            raise ValueError("Input must be a JSON string or a JSON object.")

        if "user_input_script" in data:
            data["Scheduled Task Script"] = data.pop("user_input_script")

            # Replace account_id with account_name from database
            if "account_id" in data:
                account_name = get_account_name(data["account_id"])
                data["account_name"] = account_name
                del data["account_id"]

            # Define key replacements
            replacements = {"account_name": "Account Name", "db_name_app": "App Database", "_id": "Task ID"}

            # Apply key replacements
            data = replace_keys(data, replacements)

            formatted_result = ""
            
            if "Account Name" in data:
                formatted_result += f"<b>Account Name</b>: {data['Account Name']}<br>"
                del data["Account Name"]
                
            if "App Database" in data:
                formatted_result += f"<b>App Database</b>: {data['App Database']}<br>"
                del data["App Database"]
                
            if "Task ID" in data:
                formatted_result += f"<b>Task ID</b>: {data['Task ID']}<br>"
                del data["Task ID"]    
            
            for key, value in data.items():
                if key == "Scheduled Task Script":
                    formatted_result += f"<details><summary><b>{key}</b></summary>{value}</details>"
                else:
                    formatted_result += f"<b>{key}</b>: {value}<br>"
        else:
            formatted_result = ""
            for key, value in data.items():
                formatted_result += f"<b>{key}</b>: {value}<br>"

        return formatted_result
    except json.JSONDecodeError as e:
        return f"Error decoding JSON: {e}"

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/translate', methods=['POST'])
def translate():
    try:
        sumo_data = request.form['sumo_data']
        formatted_result = parse_sumo_data(sumo_data)
        return render_template('result.html', result=formatted_result)
    except Exception as e:
        return f"Error during translation: {e}"

if __name__ == '__main__':
    app.run(debug=True)
